const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');
const app = express();
const port = 4000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//allow all cors requests
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// get config vars
dotenv.config();

// access config var
process.env.TOKEN_SECRET;

function generateAccessToken(party) {
    const payload = {
        'https://daml.com/ledger-api': {
          'ledgerId': 'sandbox',
          'applicationId': 'foobar',
          'actAs': [
            party
          ]
        }
      }
    return jwt.sign(payload, process.env.TOKEN_SECRET, { algorithm: 'HS256' });
  }

app.post('/token', (req, res) => {
    console.log(req)
    const token = generateAccessToken(req.body.party);
    console.log(token);
    res.json({'token': token});

});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })